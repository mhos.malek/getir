## intro

This is a code challenge for Getir Frontend role. I hope you enjoy it!

## FRONT END and Backend

This repo has two apps actually.

## FE

one is the frontend app that is bootstrapped with CRA and use port 4000 (and of course your desired port if you use docker files or docker compose inside the client folder)

## BE

The other project is a NestJS project that provide API and data for frontend. the Data is the json files that you gave me (so no DB in the nestJS) app. the BE app is also simple and not including many things. just providing simple data.

# client App details

Tools:

- React
- Redux
- Redux Saga
- Typescript (not fully typed the app but just to show that I have the knowledege of the typescript)
- StyledComponents
- React Router
- Jest
- React testing library
- Ngnix (for production use only)
- Docker
- AWS to serve the app on cloud (EC2 servers)
- linter/husky

## Folder structure

A quick look at the files and directories you'll see in the current project.

    ├── client <Frontend>
    ├─── public
    ├─── .gitignore
    ├─── package.json
    ├─── .env (the App configs)
    ├─── src
    ├──── App.tsx
    ├──── App.spec.tsx
    ├──── index.tsx
    ├──── setupTests.ts
    ├──── store
    ├────── store.ts (redux store)
    ├────── hooks.ts (typed hooks)
    ├────── root-sagas.ts (sagas)
    ├──── services (Api calls and http clients)
    ├──── providers (Api calls and http clients)
    ├────── error-provider (error bundary and show case of how we can handle error but not completed)
    ├──── theme-provider (configure the themes and styled component realted things)
    ├──────── default theme
    ├──────── default styles
    ├──────── styled-component realted types and configs
    ├────── translation-provider
    ├──── utils (general utls that are not realted to a feature)
    ├──── features
    ├────── (feature-name)
    ├────────── ui (pages/screen/Ui components)
    ├────────── components (shared or common components realted to a specefic feature)
    ├────────── redux (redux data or logical datas)
    ├────────── data (data related to a feature)
    ├────────── locales (translations of the feature)
    ├────────── utils (utils realted to the feature)
    ├────────── routes (routes of the feature - they are all combined at the main router file in root)
    ├────────── tests

    ├──── components
    ├────── (base-components)/common(not a feature-components)-layout component
    ├────────── Dockerfile - for local development
    ├────────── Dockerfile.prod - for production env

    └─── README.md

## Dependencies

- [React](https://github.com/facebook/react):
- [Redux](https://redux.js.org/)
- [Redux Saga](https://redux-saga.js.org/)
- [Styled Components](https://styled-components.com/)
- [react-testing-library](https://testing-library.com/docs/react-testing-library/intro/)
- [React Router](https://reactrouter.com/)
- Typescript (not fully typed the app but just to show that I have the knowledege of the typescript)
- Jest
- React testing library
- Ngnix (for production use only)
- Docker
- AWS
- linter/husky

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

# Backend App details

It's not a great app! it has not fully used the benefits of NestJS and I just used it to have API or a data provider. But I included the filtering process of data and query parameter handlers from API in there.

# Dockerfile

you can run both client and API in docker files. the related files are provided inside the directories but if you want to run them in paralel (so no need to run each of them seperatly) you can use the docker-compose file that is placed inside the root directory. this one will start both apps in same time:) have fun!

# Deployments: 

http://3.66.130.43/
NOTE: this is only http! 



# TODO:

This is a code challenge and I have very limited time to do all the things but If it was production ready app and I had more time I would like to do this improvemnts:

### Backend APP:
- Add DTO to API so frontend can expect proper data from BE.
- improve the controllers and services (right now it's all in one place)
- fully type the app (no typechecking there as there was limited time to make it work)

### Frontend APP

- Fix typescript type checking. fully add typechecking to all API (with provided DTO From server) and add type to all the components (right now I just added some types to let you know that I am working with TS but hadn't time to fully type everything)
- add Functional tests using (cypress)
- add translations in FE (i18n as we use in our current project and use translation managment tools)
- improve styles and make it more consistent (more usage of theme and predefined styles from StyledComponent theme feature)
- more documentions



That's it! Hope it still OK in the review process!
