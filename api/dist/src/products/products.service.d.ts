export declare class ProductService {
    getAllProducts(query: any): any;
    sortItems(products: any, sortQuery: any): Array<any>;
    filterItems(filters: any): Array<any>;
    getAllTags(filters: any): Array<any>;
}
