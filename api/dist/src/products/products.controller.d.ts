import { ProductService } from "./products.service";
export declare class ProductsController {
    private readonly productService;
    constructor(productService: ProductService);
    getAllProducts(query: any): Array<any>;
}
