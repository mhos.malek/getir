"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EOrderTypes = exports.ESortType = void 0;
var ESortType;
(function (ESortType) {
    ESortType["PRICE"] = "price";
    ESortType["DATE"] = "date";
})(ESortType || (ESortType = {}));
exports.ESortType = ESortType;
var EOrderTypes;
(function (EOrderTypes) {
    EOrderTypes["ASCENDING"] = "ascending";
    EOrderTypes["DESCENDING"] = "descending";
})(EOrderTypes || (EOrderTypes = {}));
exports.EOrderTypes = EOrderTypes;
//# sourceMappingURL=sortTypes.js.map