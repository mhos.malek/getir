declare enum ESortType {
    PRICE = "price",
    DATE = "date"
}
declare enum EOrderTypes {
    ASCENDING = "ascending",
    DESCENDING = "descending"
}
export { ESortType, EOrderTypes };
