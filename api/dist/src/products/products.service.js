"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductService = void 0;
const common_1 = require("@nestjs/common");
const mockData = require("../../data.json");
const sortTypes_1 = require("./sortTypes");
function paginateUtils(arr, size) {
    return arr.reduce((acc, val, i) => {
        let idx = Math.floor(i / size);
        let page = acc[idx] || (acc[idx] = []);
        page.push(val);
        return acc;
    }, []);
}
const sleep = (time) => {
    return new Promise((resolve) => setTimeout(resolve, time));
};
let ProductService = class ProductService {
    async getAllProducts(query) {
        const { page, brand, tag, itemType, sort, order } = query;
        const filtersQuery = { brand, tag, itemType };
        let filteredProducts = [];
        if (brand === null || brand === void 0 ? void 0 : brand.includes("All")) {
            delete filtersQuery.brand;
        }
        if (tag === null || tag === void 0 ? void 0 : tag.includes("All")) {
            delete filtersQuery.tag;
        }
        if (Object.values(filtersQuery).some((item) => item !== undefined)) {
            filteredProducts = this.filterItems(filtersQuery);
        }
        else {
            filteredProducts = mockData;
        }
        const sortQuery = {
            type: sort || sortTypes_1.ESortType.DATE,
            order: order || sortTypes_1.EOrderTypes.ASCENDING,
        };
        const sortedAndFilteredProducts = this.sortItems(filteredProducts, sortQuery);
        let page_size = 16;
        const allPages = paginateUtils(sortedAndFilteredProducts, page_size);
        const numberedPage = page ? Number(page) : 0;
        const allItemsLength = sortedAndFilteredProducts === null || sortedAndFilteredProducts === void 0 ? void 0 : sortedAndFilteredProducts.length;
        const pagination = {
            allPages: allPages.length - 1,
            allItemsLength,
            pageSize: page_size,
        };
        await sleep(1500);
        return { data: allPages[page - 1 || 0], pagination };
    }
    sortItems(products, sortQuery) {
        const sortBasedOnPriceAscending = (a, b) => parseFloat(a.price) - parseFloat(b.price);
        const sortBasedOnPriceDescending = (a, b) => parseFloat(b.price) - parseFloat(a.price);
        const sortBasedOnDateAscending = (a, b) => parseFloat(a.added) - parseFloat(b.added);
        const sortBasedOnDateDescending = (a, b) => parseFloat(b.added) - parseFloat(a.added);
        let data = [];
        if (sortQuery.type === sortTypes_1.ESortType.DATE) {
            if (sortQuery.order === sortTypes_1.EOrderTypes.ASCENDING) {
                data = products.sort(sortBasedOnDateAscending);
            }
            if (sortQuery.order === sortTypes_1.EOrderTypes.DESCENDING) {
                data = products.sort(sortBasedOnDateDescending);
            }
        }
        if (sortQuery.type === sortTypes_1.ESortType.PRICE) {
            if (sortQuery.order === sortTypes_1.EOrderTypes.ASCENDING) {
                data = products.sort(sortBasedOnPriceAscending);
            }
            if (sortQuery.order === sortTypes_1.EOrderTypes.DESCENDING) {
                data = products.sort(sortBasedOnPriceDescending);
            }
        }
        return data;
    }
    filterItems(filters) {
        return mockData.filter((productItem) => {
            var _a;
            return (productItem.tags.some((tag) => { var _a; return (_a = filters.tag) === null || _a === void 0 ? void 0 : _a.includes(tag); }) ||
                ((_a = filters.brand) === null || _a === void 0 ? void 0 : _a.includes(productItem.manufacturer)) ||
                filters.itemType === productItem.itemType);
        });
    }
    getAllTags(filters) {
        return mockData.filter((productItem) => {
            var _a;
            return (productItem.tags.some((tag) => { var _a; return (_a = filters.tag) === null || _a === void 0 ? void 0 : _a.includes(tag); }) ||
                ((_a = filters.brand) === null || _a === void 0 ? void 0 : _a.includes(productItem.manufacturer)) ||
                filters.itemType === productItem.itemType);
        });
    }
};
ProductService = __decorate([
    (0, common_1.Injectable)()
], ProductService);
exports.ProductService = ProductService;
//# sourceMappingURL=products.service.js.map