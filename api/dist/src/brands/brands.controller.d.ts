import { BrandsService } from "./brands.service";
export declare class BrandsController {
    private readonly brandsService;
    constructor(brandsService: BrandsService);
    getAllBrands(): Array<any>;
}
