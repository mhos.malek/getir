import { Module } from "@nestjs/common";
import { AppController } from "./app.controller";
import { ProductsController } from "./products/products.controller";
import { ProductService } from "./products/products.service";
import { BrandsController } from './brands/brands.controller';
import { BrandsService } from "./brands/brands.service";

@Module({
  imports: [],
  controllers: [AppController, ProductsController, BrandsController],
  providers: [ProductService, BrandsService],
})
export class AppModule {}
