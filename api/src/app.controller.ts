import { Controller, Get } from "@nestjs/common";

@Controller()
export class AppController {
  @Get()
  showAPIGuideline(): string {
    return "API: /products and /brands";
  }
}
