enum ESortType {
  PRICE = 'price',
  DATE = 'date',
}

enum EOrderTypes {
  ASCENDING = 'ascending',
  DESCENDING = 'descending',
}

export { ESortType, EOrderTypes };
