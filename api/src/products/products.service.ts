import { Injectable } from "@nestjs/common";
import * as mockData from "../../data.json";
import { EOrderTypes, ESortType } from "./sortTypes";

// There is no validation, no DB and nothing! it's simple thing for FE task

function paginateUtils(arr, size) {
  return arr.reduce((acc, val, i) => {
    let idx = Math.floor(i / size);
    let page = acc[idx] || (acc[idx] = []);
    page.push(val);

    return acc;
  }, []);
}

const sleep = (time) => {
  return new Promise((resolve) => setTimeout(resolve, time));
};

@Injectable()
export class ProductService {
  // @ts-ignore
  async getAllProducts(query): any {
    const { page, brand, tags, itemType, sort, order } = query;
    const filtersQuery = { brand, tags, itemType };

    let filteredProducts = [];

    if (brand?.includes("All")) {
      delete filtersQuery.brand;
    }

    console.log(tags);
    if (tags?.includes("All")) {

      delete filtersQuery.tags;
    }

    if (Object.values(filtersQuery).some((item) => item !== undefined)) {
      filteredProducts = this.filterItems(filtersQuery);
    } else {
      filteredProducts = mockData;
    }

    const sortQuery = {
      type: sort || ESortType.DATE,
      order: order || EOrderTypes.ASCENDING,
    };

    const sortedAndFilteredProducts = this.sortItems(
      filteredProducts,
      sortQuery
    );

    let page_size = 16;

    const allPages = paginateUtils(sortedAndFilteredProducts, page_size);

    const numberedPage = page ? Number(page) : 0;
    const allItemsLength = sortedAndFilteredProducts?.length;

    const pagination = {
      allPages: allPages.length - 1,
      allItemsLength,
      pageSize: page_size,
    };

    await sleep(1500);
    return { data: allPages[page - 1 || 0], pagination };
  }
  sortItems(products, sortQuery): Array<any> {
    const sortBasedOnPriceAscending = (a, b) =>
      parseFloat(a.price) - parseFloat(b.price);
    const sortBasedOnPriceDescending = (a, b) =>
      parseFloat(b.price) - parseFloat(a.price);

    const sortBasedOnDateAscending = (a, b) =>
      parseFloat(a.added) - parseFloat(b.added);
    const sortBasedOnDateDescending = (a, b) =>
      parseFloat(b.added) - parseFloat(a.added);

    let data = [];
    if (sortQuery.type === ESortType.DATE) {
      if (sortQuery.order === EOrderTypes.ASCENDING) {
        data = products.sort(sortBasedOnDateAscending);
      }
      if (sortQuery.order === EOrderTypes.DESCENDING) {
        data = products.sort(sortBasedOnDateDescending);
      }
    }

    if (sortQuery.type === ESortType.PRICE) {
      if (sortQuery.order === EOrderTypes.ASCENDING) {
        data = products.sort(sortBasedOnPriceAscending);
      }
      if (sortQuery.order === EOrderTypes.DESCENDING) {
        data = products.sort(sortBasedOnPriceDescending);
      }
    }

    return data;
  }
  filterItems(filters): Array<any> {
    return mockData.filter((productItem) => {
      return (
        productItem.tags.some((tag) => filters.tags?.includes(tag)) ||
        filters.brand?.includes(productItem.manufacturer) ||
        filters.itemType === productItem.itemType
      );
    });
  }
  getAllTags(filters): Array<any> {
    return mockData.filter((productItem) => {
      return (
        productItem.tags.some((tag) => filters.tag?.includes(tag)) ||
        filters.brand?.includes(productItem.manufacturer) ||
        filters.itemType === productItem.itemType
      );
    });
  }
}
