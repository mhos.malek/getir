import { Controller, Get, Query } from "@nestjs/common";
import { ProductService } from "./products.service";

@Controller("products")
export class ProductsController {
  constructor(private readonly productService: ProductService) {}
  @Get()
  getAllProducts(@Query() query): Array<any> {
    return this.productService.getAllProducts(query);
  }
}
