import { Injectable } from "@nestjs/common";
import * as mockData from "../../companies.json";

@Injectable()
export class BrandsService {
  getAllBrands(): Array<any> {
    return mockData;
  }
}
