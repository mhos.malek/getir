import Pathes from "./pathes";
// pages
import ProductsRoutes from "../features/products/routes/routes";
import BrandsRoutes from "../features/brands/routes/routes";
import BasketRoutes from "../features/basket/routes/routes";

const NotFoundPage = () => null;
const routes = [
  ...ProductsRoutes,
  ...BrandsRoutes,
  ...BasketRoutes,

  // not found
  {
    path: Pathes.NOT_FOUND_PATH,
    component: NotFoundPage,
    exact: false,
  },
];

export default routes;
