import React from "react";
import { Route, BrowserRouter, Routes } from "react-router-dom";
import routes from "./routes";

const RouterProvider: React.FC = () => (
  <BrowserRouter>
    <Routes>
      {routes.map((route, index) => (
        <Route
          key={index}
          path={route.path}
          element={<route.component />}
        ></Route>
      ))}
    </Routes>
  </BrowserRouter>
);
export default RouterProvider;
