import React from 'react';
import { ErrorBoundaryProps, ErrorBoundaryState } from './types';

class ErrorBoundary extends React.Component<
  ErrorBoundaryProps,
  ErrorBoundaryState
> {
  constructor(props: ErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false, message: 'error' };
  }

  static getDerivedStateFromError(error: any) {
    return { hasError: true, message: error.message || '' };
  }

  componentDidCatch() {
    console.log('error happend')
    // or send to error management tool or logger like sentry
  }

  render() {
    const { message, hasError } = this.state;
    if (hasError) {
      return <p>{message}</p>;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;
