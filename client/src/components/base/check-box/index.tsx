import { Button } from "./styles";
export type TCheckboxProps = {
  label: string;
  selected: boolean;
  handler: (slug: string) => void;
  slug?: string;
};
const Checkbox: React.FC<TCheckboxProps> = ({
  label = "",
  selected = false,
  handler,
  slug = "",
}) => {
  return (
    <Button
      className={selected ? "selected" : ""}
      onClick={() => handler(slug)}
    >
      <span className="checkbox">
        <img src="/images/checkbox.svg" alt="checkbox" />
      </span>
      <span>{label}</span>
    </Button>
  );
};

export default Checkbox;
