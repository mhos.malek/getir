/* eslint-disable testing-library/no-node-access */
/* eslint-disable testing-library/no-container */
import { render, fireEvent, screen } from "@testing-library/react";
import Checkbox from ".";

describe("Checkbox Component", () => {
  test("renders with props", () => {
    const mockFn = jest.fn();
    const { container } = render(
      <Checkbox label="name" slug="slug" selected={true} handler={mockFn} />
    );
    expect(container).toBeTruthy();
    // eslint-disable-next-line testing-library/no-node-access
    expect(container.querySelector(".selected")).toBeTruthy();
    expect(screen.getByText(/name/)).toBeTruthy();
    const button = container.querySelector("button");
    fireEvent.click(button);
    expect(mockFn).toHaveBeenCalledWith("slug");
  });
});
