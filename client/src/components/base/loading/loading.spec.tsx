/* eslint-disable testing-library/no-container */
/* eslint-disable testing-library/no-node-access */
import { render } from "@testing-library/react";
import Loading from ".";

describe("Loading Component", () => {
  test("renders with props", () => {
    const { container } = render(<Loading />);
    expect(container).toBeTruthy();
  });
});
