import { LoadingWrapper } from "./styles";

const Loading: React.FC = () => {
  return (
    <LoadingWrapper>
      <div></div>
      <div></div>
    </LoadingWrapper>
  );
};

export default Loading;
