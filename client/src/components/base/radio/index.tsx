import { Button } from "./styles";
export type TRadioProps = {
  label: string;
  selected: boolean;
  onClick: (slug: string) => void;
  slug: string;
};
const Radio: React.FC<TRadioProps> = ({
  label = "",
  selected = false,
  onClick,
  slug = "",
}) => (
  <Button className={selected ? "selected" : ""} onClick={() => onClick(slug)}>
    <span className="radio">
      <img src="/images/radio.svg" alt="radio" />
    </span>
    <span>{label}</span>
  </Button>
);
export default Radio;
