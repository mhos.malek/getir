/* eslint-disable testing-library/no-container */
/* eslint-disable testing-library/no-node-access */
import { render, fireEvent, screen } from "@testing-library/react";
import Radio from ".";

describe("Radio Component", () => {
  test("renders with props", () => {
    const mockFn = jest.fn();
    const { container } = render(
      <Radio label="name" slug="slug" selected={false} onClick={mockFn} />
    );
    expect(container).toBeTruthy();
    expect(container.querySelector(".selected")).toBeFalsy();
    expect(screen.getByText(/name/)).toBeTruthy();
    const button = container.querySelector("button");
    fireEvent.click(button);
    expect(mockFn).toHaveBeenCalledWith("slug");
  });
});
