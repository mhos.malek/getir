import React from "react";

export type TLayoutHeaderProps = {
  children: React.ReactNode;
};
