// import NarbarBasket from "../../../features/basket/navbar-basket";
import { Navbar, Logo, NavbarContainer } from "./styles";
import { TLayoutHeaderProps } from "./model";

const LayoutHeader: React.FC<TLayoutHeaderProps> = ({ children }) => {
  return (
    <Navbar>
      <NavbarContainer className="container">
        <Logo href="">
          <img src="/images/logo.svg" alt="Market" />
        </Logo>
        {children}
      </NavbarContainer>
    </Navbar>
  );
};

export default LayoutHeader;
