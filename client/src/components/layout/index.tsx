import React from "react";
import LayoutFooter from "./LayoutFooter";
import LayoutHeader from "./LayoutHeader";
import { MainContainer } from "./styles";

const PagesLayout: React.FC<{
  children: React.ReactNode;
  header?: React.ReactNode;
  footer?: React.ReactNode;
}> = ({ children, header, footer }) => {
  return (
    <>
      <LayoutHeader>{header}</LayoutHeader>
      <MainContainer className="container">{children}</MainContainer>
      <LayoutFooter>
        <ul>
          <li>©2019 Market</li>
          <li>Privacy Policy</li>
        </ul>
      </LayoutFooter>
    </>
  );
};

export default PagesLayout;
