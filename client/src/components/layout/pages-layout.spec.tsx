import { render, screen } from "@testing-library/react";
import { Provider as ReduxProvider } from "react-redux";
import store from "../../store";
import PagesLayout from ".";

describe("PagesLayout Component", () => {
  test("renders with props", () => {
    const { container } = render(
      <ReduxProvider store={store}>
        <PagesLayout>hello world</PagesLayout>
      </ReduxProvider>
    );
    expect(container).toBeTruthy();
    expect(screen.getByText(/hello world/)).toBeTruthy();
  });
});
