import { render, screen } from "@testing-library/react";
import Footer from ".";

describe("Footer Component", () => {
  test("renders with props", () => {
    const { container } = render(<Footer />);
    expect(container).toBeTruthy();
    expect(screen.getByText(/footer/)).toBeTruthy();
  });
});
