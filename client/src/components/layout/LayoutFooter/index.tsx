import React from "react";
import FooterContainer from "./styles";

const LayoutFooter: React.FC<{ children?: React.ReactNode }> = ({
  children,
}) => {
  return (
    <FooterContainer>
      <footer className="container">{children}</footer>
    </FooterContainer>
  );
};

export default LayoutFooter;
