import { useState } from "react";
import { Box } from "../../../features/products/components/products-list/styles";
import { FilterBoxWrapper, Title } from "./styles";

export type TFilterBoxProps = {
  title: string;
  children: React.ReactNode;
};
const FilterBox: React.FC<TFilterBoxProps> = ({ title, children }) => {
  const [show, setShow] = useState(false);
  return (
    <FilterBoxWrapper>
      <Title onClick={() => setShow(!show)}>
        <span>{title}</span>{" "}
      </Title>
      <Box className={`box ${show && "open"}`}>{children}</Box>
    </FilterBoxWrapper>
  );
};

export default FilterBox;
