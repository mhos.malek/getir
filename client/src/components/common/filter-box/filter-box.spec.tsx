import { render, screen } from "@testing-library/react";
import FilterBox from ".";

describe("FilterBox Component", () => {
  test("renders with props", () => {
    const { container } = render(
      <FilterBox title="hello">hello world</FilterBox>
    );
    expect(container).toBeTruthy();
    expect(screen.getByText(/hello world/)).toBeTruthy();
  });
});
