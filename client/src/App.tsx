import React from "react";
// providers
import ErrorBoundary from "./error-boundary";
import Router from "./router";
import ThemeProvider from "./theme-provider";
import { Helmet } from "react-helmet";
import { Provider } from "react-redux";
import store from "./store";

const App: React.FC = () => (
  <ErrorBoundary>
    <Provider store={store}>
      <ThemeProvider>
        <Helmet>
          <meta charSet="utf-8" />
          <title>Getir Code Challenge FrontEnd</title>
        </Helmet>
        <Router />
      </ThemeProvider>
    </Provider>
  </ErrorBoundary>
);

export default App;
