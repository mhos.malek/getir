import { mediaQueriesMaxWidth, mediaQueriesMinWidth } from "./responsive";
import { createGlobalStyle } from "styled-components";

// Add normalize css
import "normalize.css";


const scrollBar = `body::-webkit-scrollbar {
  width: 1em;
}
 
body::-webkit-scrollbar-track {
  box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
}
 
body::-webkit-scrollbar-thumb {
  background-color: darkgrey;
  outline: 1px solid slategrey;
}`;

const defaultBody = ` html {
  font-size: 16px;
}
a {
  text-decoration: none;
}
body {
  background-color: #e5e5e5;
  color: #525252;
}
`;
const GlobalDefaultStyles = createGlobalStyle`

  ${scrollBar}
${defaultBody}
 
 
* {
  box-sizing: border-box;
}
     
body {
  font-family: 'Open Sans'!important;
}
    
      .container{
          margin: 0 auto;
          width: 100%;
          max-width: 1240px;
          display: flex;
      }
      h1{
        color:#6F6F6F;
        font-size: 20px;
        line-height: 26px;
        font-weight: 500;
        padding: 0;
        margin: 0;
      }
    
      
    
`;



export default GlobalDefaultStyles;
