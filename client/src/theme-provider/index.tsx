import React from "react";
import { ThemeProviderProps } from "./types";
import GlobalDefaultStyles from "./themes/default";

const ThemeProvider: React.FC<ThemeProviderProps> = ({ children }) => (
  <>
    <GlobalDefaultStyles />
    {children}
  </>
);

export default ThemeProvider;
