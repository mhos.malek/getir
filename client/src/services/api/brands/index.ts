import { HttpMethod } from "../../../utils/http-client";
import httpClient from "../../http-clients/default-client";

const getAllBrandsApi = () => {
  return httpClient.request(HttpMethod.GET, "/brands");
};

export default getAllBrandsApi;
