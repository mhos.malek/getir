import { HttpMethod } from "../../../utils/http-client";
import httpClient from "../../http-clients/default-client";

const getProductsListApi = (query: string) => httpClient.request(HttpMethod.GET, query);

export default getProductsListApi;
