import HttpClient from "../../../utils/http-client";

/**
 * http client that interacts with
 */

const httpClient = new HttpClient(process.env.REACT_APP_BASE_APT_URL);

export default httpClient;
