export function calculateTotalAmount(array) {
  let amount = 0;
  if (array.length > 0) {
    array.forEach((element) => {
      amount += element.count * element.price;
    });
  }
  return amount.toFixed(2);
}

export const addPropToArray = (array, selectedArray) => {
  const obj = {};
  const output = [...array];

  if (selectedArray.length > 0) {
    selectedArray.forEach((item) => {
      obj[item] = item;
    });
  }
  output.forEach((item) => {
    item.selected = item.slug === obj[item.slug];
  });

  return output;
}
