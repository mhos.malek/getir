import { all } from "redux-saga/effects";
import createSagaMiddleware from "redux-saga";
import productSaga from "../features/products/redux/sagas";
import brandsSaga from "../features/brands/redux/sagas";
import basketSaga from "../features/basket/redux/sagas";

const sagaMiddleware = createSagaMiddleware();

function* sagas() {
  yield all([productSaga(), brandsSaga(), basketSaga()]);
}

export { sagaMiddleware, sagas };
