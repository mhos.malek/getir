import {
  createStore,
  combineReducers,
  Store,
  compose,
  applyMiddleware,
} from "redux";
import { sagaMiddleware, sagas } from "./root-sagas";

import { composeWithDevTools } from "redux-devtools-extension";

import { reducers as basketReducers } from "../features/basket/redux/reducers";
import { reducers as brandsReducers } from "../features/brands/redux/reducers";
import { reducers as filterReducers } from "../features/filter/redux/reducer";
import { reducers as productsReducer } from "../features/products/redux/reducers";

const rootReducer = combineReducers({
  basket: basketReducers,
  brands: brandsReducers,
  filter: filterReducers,
  products: productsReducer,
});

const store: Store = createStore(
  rootReducer,
  compose(composeWithDevTools(applyMiddleware(sagaMiddleware)))
);

sagaMiddleware.run(sagas);

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
export default store;
