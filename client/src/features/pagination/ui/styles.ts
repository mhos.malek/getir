import styled from "styled-components";

export const PaginationWrapper = styled.ul`
  display: flex;
  list-style-type: none;
`;

export const PaginationItem = styled.li<any>`
  padding: 0 9px;
  height: 32px;
  text-align: center;
  margin: auto 4px;
  display: flex;
  box-sizing: border-box;
  align-items: center;
  letter-spacing: 0.01071em;
  line-height: 1.43;
  font-size: 16px;

  ${({ dots }) =>
    dots
      ? ` &:hover {
    background-color: transparent;
    cursor: default;
  }`
      : ``}

  &:hover {
    transition: all 0.3s;
    color: #1ea4ce;
    cursor: pointer;
  }

  ${({ selected }) =>
    selected
      ? `   
    background-color: #1EA4CE;
    color: white;
    border-radius: 0;`
      : ``}

  .arrow {
    &::before {
      position: relative;
      /* top: 3pt; Uncomment this to lower the icons as requested in comments*/
      content: "";
      /* By using an em scale, the arrows will size with the font */
      display: inline-block;
      width: 0.4em;
      height: 0.4em;
      border-right: 0.12em solid rgba(0, 0, 0, 0.87);
      border-top: 0.12em solid rgba(0, 0, 0, 0.87);
    }

    &.left {
      transform: rotate(-135deg) translate(-50%);
    }

    &.right {
      transform: rotate(45deg);
    }
  }

  ${({ disabled }) =>
    disabled
      ? ` pointer-events: none;



  span {
    color: rgba(0, 0, 0, 0.43);
  }

  &:hover {
    background-color: transparent;
    cursor: default;
  }`
      : ``}
`;

export const ArrowLeft = styled.i`
  box-sizing: border-box;
  position: relative;
  display: block;
  transform: scale(var(--ggs, 1));
  width: 22px;
  height: 22px;
  &:after,
  &:before {
    content: "";
    display: block;
    box-sizing: border-box;
    position: absolute;
    left: 3px;
  }
  &:after {
    width: 8px;
    height: 8px;
    border-bottom: 2px solid #1ea4ce;
    border-left: 2px solid #1ea4ce;
    transform: rotate(45deg);
    bottom: 7px;
  }
  &:before {
    width: 16px;
    height: 2px;
    bottom: 10px;
    background: #1ea4ce;
  }
`;

export const ArrowRight = styled.i`
  box-sizing: border-box;
  position: relative;
  display: block;
  transform: scale(var(--ggs, 1));
  width: 22px;
  height: 22px;
  &:after,
  &:before {
    content: "";
    display: block;
    box-sizing: border-box;
    position: absolute;
    right: 3px;
  }
  &:after {
    width: 8px;
    height: 8px;
    border-top: 2px solid #1ea4ce;
    border-right: 2px solid #1ea4ce;
    transform: rotate(45deg);
    bottom: 7px;
  }
  &:before {
    width: 16px;
    height: 2px;
    bottom: 10px;
    background: #1ea4ce;
  }
`;

export const NextPrevItem = styled.span`
  margin: 0 8px;
  color: #1ea4ce;
`;

export const PaginationItemNextPrevWrapper = styled(PaginationItem)`
  display: none;
  @media (min-width: 470px) {
    display: flex;
  }
`;
