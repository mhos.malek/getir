import { usePagination, DOTS } from "../hook/usePagination";
import {
  ArrowLeft,
  ArrowRight,
  NextPrevItem,
  PaginationItem,
  PaginationItemNextPrevWrapper,
  PaginationWrapper,
} from "./styles";
const Pagination = (props) => {
  const {
    onPageChange,
    totalCount,
    siblingCount = 1,
    currentPage,
    pageSize,
  } = props;

  const paginationRange = usePagination({
    currentPage,
    totalCount,
    siblingCount,
    pageSize,
  });

  if (!paginationRange || currentPage === 0 || paginationRange.length < 2) {
    return null;
  }

  const onNext = () => {
    onPageChange(currentPage + 1);
  };

  const onPrevious = () => {
    onPageChange(currentPage - 1);
  };

  let lastPage = paginationRange[paginationRange.length - 1];
  return (
    <PaginationWrapper>
      <PaginationItemNextPrevWrapper
        disabled={currentPage === 1}
        onClick={onPrevious}
      >
        <ArrowLeft />
        <NextPrevItem>Prev</NextPrevItem>
      </PaginationItemNextPrevWrapper>
      {paginationRange.map((pageNumber) => {
        // If the pageItem is a DOT, render the DOTS unicode character
        if (pageNumber === DOTS) {
          return (
            <PaginationItem key={pageNumber} dots>
              &#8230;
            </PaginationItem>
          );
        }

        return (
          <PaginationItem
            key={pageNumber}
            selected={pageNumber === currentPage}
            onClick={() => onPageChange(pageNumber)}
          >
            {pageNumber}
          </PaginationItem>
        );
      })}

      {/*  Right Navigation arrow */}
      <PaginationItemNextPrevWrapper
        disabled={currentPage === lastPage}
        className={"pagination-item"}
        onClick={onNext}
      >
        <NextPrevItem>Next</NextPrevItem>
        <ArrowRight />
      </PaginationItemNextPrevWrapper>
    </PaginationWrapper>
  );
};

export default Pagination;
