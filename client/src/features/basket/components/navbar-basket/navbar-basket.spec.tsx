/* eslint-disable testing-library/prefer-presence-queries */
import { render, screen } from "@testing-library/react";
import NarbarBasketComponent from "./navbar-basket";

describe("NarbarBasket Component", () => {
  test("renders with props", () => {
    const { container } = render(<NarbarBasketComponent />);
    expect(container).toBeTruthy();
    expect(screen.getByText(/₺46.79/)).toBeFalsy();
  });
});
