import { useSelector } from "react-redux";
import { selectTotal, selectTotalAmount } from "../../redux/selector";
import { Button } from "./styles";

function NarbarBasketComponent() {
  const amount = useSelector(selectTotal);
  return (
    <Button>
      <img src="/images/basket.svg" alt="basket" />
      <span>₺{Number(amount)}</span>
    </Button>
  );
}

export default NarbarBasketComponent;
