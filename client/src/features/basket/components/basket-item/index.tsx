import { useDispatch } from "react-redux";
import { addToBasket, removeFromasket } from "../../redux/actions";
import { Row, ITemInfo, Name, Price, ITemForm, Button, Count } from "./styles";

const BasketItem: React.FC<any> = ({
  name = "",
  price = "",
  count = 1,
  slug = "",
}) => {
  const dispatch = useDispatch();
  const handleRemoveItemFromBasket = () =>
    dispatch(removeFromasket({ name, price, count, slug }));
  const handleAddItemFromBasket = () =>
    dispatch(addToBasket({ name, price, count, slug }));

  return (
    <Row className="basket-item">
      <ITemInfo>
        <Name>{name}</Name>
        <Price>₺ {price}</Price>
      </ITemInfo>
      <ITemForm>
        <Button onClick={handleRemoveItemFromBasket} className="decrease">
          <img src="/images/decrease.svg" alt="decrease" />
        </Button>
        <Count>{count}</Count>
        <Button onClick={handleAddItemFromBasket} className="increase">
          <img src="/images/increase.svg" alt="increase" />
        </Button>
      </ITemForm>
    </Row>
  );
};

export default BasketItem;
