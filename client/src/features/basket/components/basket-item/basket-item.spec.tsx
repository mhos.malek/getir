/* eslint-disable testing-library/no-node-access */
/* eslint-disable testing-library/no-container */
import { render, fireEvent, screen } from "@testing-library/react";
import BasketItem from ".";

describe("BasketItem Component", () => {
  test("renders with props", () => {
    const mockFn = jest.fn();
    const { container } = render(
      <BasketItem
        name="name"
        slug="slug"
        price="46.79"
        count={16}
        handler={mockFn}
      />
    );
    expect(container).toBeTruthy();
    expect(screen.getByText(/name/)).toBeTruthy();
    expect(screen.getByText(/₺ 46.79/)).toBeTruthy();
    expect(screen.getByText(/16/)).toBeTruthy();
    const increase = container.querySelector(".increase");
    fireEvent.click(increase);
    expect(mockFn).toHaveBeenCalledWith({ slug: "slug" }, "increase");
    const decrease = container.querySelector(".decrease");
    fireEvent.click(decrease);
    expect(mockFn).toHaveBeenCalledWith({ slug: "slug" }, "decrease");
  });
});
