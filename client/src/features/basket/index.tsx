import { useSelector } from "react-redux";
import BasketItem from "./components/basket-item";

import { Box, Button } from "./styles";

function Basket() {
  const { items, totalAmount } = useSelector((state: any) => state.basket);

  return (
    <Box>
      {items.map((item: any) => (
        <BasketItem
          key={item.slug}
          slug={item.slug}
          name={item.name}
          price={item.price}
          count={item.count}
        />
      ))}
      {Number(totalAmount) ? (
        <Button>₺{totalAmount}</Button>
      ) : (
        "Let's Start shopping!"
      )}
    </Box>
  );
}

export default Basket;
