import { put, select, takeLatest } from "redux-saga/effects";

import ActionTypes from "./ActionTypes";
import { updateBasketAmount } from "./actions";
import { selectCurrentBasketState } from "./selector";
import { calculateTotalAmount } from "../../../utils";

export function* updateBasketSaga() {
  const basketItems: Array<any> = yield select(selectCurrentBasketState);

  yield put(updateBasketAmount(calculateTotalAmount(basketItems)));
}

function* basketSaga() {
  yield takeLatest(
    [ActionTypes.ADD_TO_BASKET, ActionTypes.REMOVE_FROM_BASKET],
    updateBasketSaga
  );
}

export default basketSaga;
