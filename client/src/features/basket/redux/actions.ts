import ActionTypes from "./ActionTypes";

export const addToBasket = (data: any) => ({
  type: ActionTypes.ADD_TO_BASKET,
  payload: data,
});

export const removeFromasket = (item: any) => ({
  type: ActionTypes.REMOVE_FROM_BASKET,
  payload: item,
});

export const updateBasketAmount = (totalAmount: any) => ({
  type: ActionTypes.UPDATE_BASKET_AMOUNT,
  payload: {
    totalAmount,
  },
});
