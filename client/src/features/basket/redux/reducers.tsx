import ActionTypes from "./ActionTypes";

const initial = {
  items: [],
  totalAmount: 0,
};

const removeFromBasket = (curentItemsInBasket: any, itemToRemove: any) => {
  const itemIndex = curentItemsInBasket.findIndex(
    (product: any) => product.slug === itemToRemove.slug
  );
  curentItemsInBasket[itemIndex].count -= 1;
  const itemAmount = curentItemsInBasket[itemIndex].count;
  if (itemAmount === 0) {
    // remove from basket if nothing letf
    return curentItemsInBasket.filter(
      (item: any) => item.slug !== itemToRemove.slug
    );
  } else {
    return curentItemsInBasket;
  }
};

const addToBasket = (currentItemsInBasket: any, newItem: any) => {
  if (currentItemsInBasket.some((item: any) => item.slug === newItem.slug)) {
    const itemIndex = currentItemsInBasket.findIndex(
      (product: any) => product.slug === newItem.slug
    );

    currentItemsInBasket[itemIndex].count += 1;

    return currentItemsInBasket;
  } else {
    return [...currentItemsInBasket, { ...newItem, count: 1 }];
  }
};
export function reducers(state = initial, action: any) {
  switch (action.type) {
    case ActionTypes.ADD_TO_BASKET:
      return { ...state, items: addToBasket(state.items, action.payload) };

    case ActionTypes.REMOVE_FROM_BASKET:
      return {
        ...state,
        items: removeFromBasket(state.items, action.payload),
      };

    case ActionTypes.UPDATE_BASKET_AMOUNT: {
      return {
        ...state,
        totalAmount: action.payload.totalAmount,
      };
    }
    default:
      return state;
  }
}
