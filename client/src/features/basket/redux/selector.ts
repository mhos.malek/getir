import { createSelector } from "reselect";
import { calculateTotalAmount } from "../../../utils";

export const selectCurrentBasketState: any = (state: any) => {
  return state.basket.items;
};

export const selectTotal = (state: any) => state.basket.totalAmount;

// to prevent calculation of total price when basket has not been changed
export const selectTotalAmount = createSelector(
  selectCurrentBasketState,
  (basketItems) => {
    return calculateTotalAmount(basketItems);
  }
);
