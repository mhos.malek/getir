/* eslint-disable testing-library/no-node-access */
/* eslint-disable testing-library/no-container */
import { render, screen } from "@testing-library/react";
import BasketComponent from "./";

describe("Basket Component", () => {
  test("renders empty basket with props", () => {
    const { container } = render(<BasketComponent />);
    expect(container).toBeTruthy();
    // eslint-disable-next-line testing-library/prefer-presence-queries
    expect(screen.getByText(/₺99.99/)).toBeFalsy();
    expect(container.querySelectorAll(".basket-item")).toHaveLength(0);
  });
  test("should render empty basket", () => {
    const { container } = render(<BasketComponent />);
    expect(container).toBeTruthy();
  });
});
