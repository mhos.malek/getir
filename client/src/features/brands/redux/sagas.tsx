import { call, put, takeLatest } from "redux-saga/effects";
import getAllBrandsApi from "../../../services/api/brands";
import { fetchListFailed, fetchListSucceed } from "./actions";

import ActionTypes from "./actionTypes";

export function* fetchBrandsListSaga() {
  try {
    const response = yield call(getAllBrandsApi);

    yield put(fetchListSucceed(response.data));
  } catch (error) {
    yield put(fetchListFailed());
  } finally {
  }
}

function* brandsSaga() {
  yield takeLatest(
    ActionTypes.BRANDS_FETCH_BRANDS_LIST,
    fetchBrandsListSaga
  );
}

export default brandsSaga;
