import { createSelector } from "reselect";

export const selectBrandsState = (state) => state.brands;
// extract brand names
export const selectBrandsName = createSelector(selectBrandsState, (brands) => {
  const tags = brands.reduce((current, res) => {
    const item = {
      name: current.name,
      slug: current.slug,
    };
    res.push(item);
    return res;
  }, []);
  return tags;
});
