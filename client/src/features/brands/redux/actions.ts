import ActionTypes from "./actionTypes";

const fetchBrandsList = () => ({
  type: ActionTypes.BRANDS_FETCH_BRANDS_LIST,
});

const fetchListSucceed = (payload) => ({
  type: ActionTypes.BRANDS_FETCH_BRANDS_LIST_SUCCEED,
  payload,
});

const fetchListFailed = () => ({
  type: ActionTypes.BRANDS_FETCH_BRANDS_LIST_FAILED,
});

export { fetchBrandsList, fetchListSucceed, fetchListFailed };
