import ActionTypes from "./actionTypes";

const initial = {
  isLoading: false,
  list: [],
};
export function reducers(state = initial, action) {
  switch (action.type) {
    case ActionTypes.BRANDS_FETCH_BRANDS_LIST:
      return {
        ...state,
        isLoading: true,
      };
    case ActionTypes.BRANDS_FETCH_BRANDS_LIST_SUCCEED:
      return { ...state, isLoading: false, list: action.payload };
    case ActionTypes.BRANDS_FETCH_BRANDS_LIST_FAILED:
      return { ...state, isLoading: false };
    default:
      return state;
  }
}
