enum ActionTypes {
  // Fetch brands
  BRANDS_FETCH_BRANDS_LIST = "BRANDS/FETCH_BRANDS_LIST",
  BRANDS_FETCH_BRANDS_LIST_SUCCEED = "BRANDS/FETCH_BRANDS_LIST_SUCCEED",
  BRANDS_FETCH_BRANDS_LIST_FAILED = "BRANDS/FETCH_BRANDS_FAILED",

}

export default ActionTypes;
