import { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { filterByBrandName } from "../../filter/redux/actions";
import { selectBrandFilter } from "../../filter/redux/selector";
import {
  selectProductsState,
} from "../../products/redux/selector";
import { fetchBrandsList } from "../redux/actions";

import BrandsComponent from "./brands";

const Brands = () => {
  const dispatch = useDispatch();

  const { list } = useSelector((state: any) => state.brands);
  const { isLoading } = useSelector(selectProductsState);
  const selectedBrands = useSelector(selectBrandFilter);

  useEffect(() => {
    dispatch(fetchBrandsList());
  }, [dispatch]);

  const setBrands = (brand) => {
    dispatch(filterByBrandName(brand));
  };

  return (
    <BrandsComponent
      list={list}
      loading={isLoading}
      selected={selectedBrands}
      handler={setBrands}
    />
  );
};

export default Brands;
