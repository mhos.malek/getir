import CheckListFilter from "../../products/components/tags/check-list-filter";
export type TBrandsComponentProps = {
  list: Array<any>;
  loading: boolean;
  selected: Array<any>;
  handler: (item: string) => void;
};
const BrandsComponent: React.FC<TBrandsComponentProps> = ({
  list = [],
  loading = false,
  selected = [],
  handler,
}) => {
  const handle = (item: string) => {
    handler(item);
  };

  return (
    <CheckListFilter
      list={list}
      loading={loading}
      selected={selected}
      handler={handle}
      title="Brands"
    />
  );
};

export default BrandsComponent;
