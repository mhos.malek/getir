import { render, screen } from "@testing-library/react";
import BrandsComponent from "./brands";

describe("Brands Component", () => {
  test("renders with props", () => {
    const { container } = render(<BrandsComponent handler={() => ""} />);
    expect(container).toBeTruthy();
    expect(screen.getByText(/Brands/)).toBeTruthy();
  });
});
