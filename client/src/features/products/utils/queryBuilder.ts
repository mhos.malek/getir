enum ESortType {
  PRICE = "price",
  DATE = "date",
}

enum EOrderTypes {
  ASCENDING = "ascending",
  DESCENDING = "descending",
}

interface IQueryBuilderPayload {
  sort: ESortType;
  order: EOrderTypes;
  brands: Array<string>;
  itemType: string;
  tags: Array<string>;
  page: string;
}
type TQueryBuilder = (queryParams: IQueryBuilderPayload) => string;
const queryBuilder: TQueryBuilder = ({
  page,
  sort,
  order,
  brands,
  itemType,
  tags,
}) => {
  let query = `/products?page=${page}`;
  // Tags
  if (tags.length) {
    tags.forEach((tag) => {
      query += `&tags=${tag}`;
    });
  }
  // Brands
  if (brands.length) {
    brands.forEach((brand) => {
      query += `&brand=${brand}`;
    });
  }
  // Item Type
  if (itemType) {
    query += `&itemType=${itemType}`;
  }
  // sort
  if (sort) {
    query  += `&sort=${sort}`;
  }
  // order 
  if(order) {
      query += `&order=${order}`;
  }

  return query;
};

export { ESortType, EOrderTypes };

export default queryBuilder;
