export const sortTypes = {
  PriceLowToHigh: {
    type: "price",
    order: "ascending",
    title: "Price low to high",
  },
  PriceHighToLow: {
    type: "price",
    order: "descending",
    title: "Price high to low",
  },
  NewToOld: {
    type: "date",
    order: "ascending",
    title: "New to old",
  },
  OldToNew: {
    type: "date",
    order: "descending",
    title: "Old to new",
  },
};
