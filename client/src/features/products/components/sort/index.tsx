import { useSelector, useDispatch } from "react-redux";
import Radio from "../../../../components/base/radio";
import FilterBox from "../../../../components/common/filter-box";
import { selectSortTypeKey } from "../../../filter/redux/selector";
import { setSelectedSort } from "../../redux/actions";
import { sortTypes } from "./types";

function SortSection() {
  const dispatch = useDispatch();
  // const selectedOrder = useSelector(selectSortOrderFilter);
  // const selectSelectedSortType = useSelector(selectSortTypeFilter);
  const selectSelectedSortKey = useSelector(selectSortTypeKey);

  const keys = Object.keys(sortTypes);

  const list = [];
  keys.forEach((key) => list.push({ key: key, ...sortTypes[key] }));

  const handleSelectSort = (sort: string) => {
    dispatch(setSelectedSort(sort));
  };

  return (
    <FilterBox title="Sorting">
      {list.map((item, index) => (
        <Radio
          key={index}
          slug={item.key}
          label={item.title}
          selected={item.key === selectSelectedSortKey}
          onClick={handleSelectSort}
        />
      ))}
    </FilterBox>
  );
}

export default SortSection;
