/* eslint-disable testing-library/no-node-access */
/* eslint-disable testing-library/no-container */
import { render } from "@testing-library/react";
import SortingComponent from "./index";

describe("Sorting Component", () => {
  test("renders with props", () => {
    const { container } = render(<SortingComponent />);
    expect(container).toBeTruthy();
  });
});
