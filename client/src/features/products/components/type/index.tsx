import { useSelector } from "react-redux";
import { selectItemTypeFilter } from "../../../filter/redux/selector";

import ItemTypesComponent from "./item-types";
// import { selectItemTypeFilter } from "../../redux/selector";

function ItemTypesFilter() {
  const selectedType = useSelector(selectItemTypeFilter);

  return <ItemTypesComponent activeType={selectedType} />;
}

export default ItemTypesFilter;
