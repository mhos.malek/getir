/* eslint-disable testing-library/no-node-access */
/* eslint-disable testing-library/no-container */
import { render, fireEvent, screen } from "@testing-library/react";
import ItemTypesComponent from "./item-types";

describe("ItemTypes Component", () => {
  test("renders with props", () => {
    const mockFn = jest.fn();
    const { container } = render(
      <ItemTypesComponent
        types={["one", "two"]}
        activeType="two"
      />
    );
    expect(container).toBeTruthy();
    expect(container.querySelector(".active")).toBeTruthy();
    expect(screen.getByText(/one/)).toBeTruthy();
    expect(container.querySelectorAll("button")).toHaveLength(2);
    expect(screen.getByText(/two/).getAttribute("class")).toContain("active");
    const button = container.querySelector("button");
    fireEvent.click(button);
    expect(mockFn).toHaveBeenCalledWith("one");
  });
});
