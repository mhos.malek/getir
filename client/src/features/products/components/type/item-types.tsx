import { useDispatch } from "react-redux";
import { filterByItemType } from "../../../filter/redux/actions";
import { Button, ItemTypesWrapper } from "./styles";

function ItemTypesComponent({ types = ["mug", "shirt"], activeType = "" }) {
  const dispatch = useDispatch();
  const handleSelectItemType = (type: string) => {
    dispatch(filterByItemType(type));
  };
  return (
    <ItemTypesWrapper>
      {types.map((type, index) => (
        <Button
          key={index}
          className={activeType === type ? "active" : ""}
          onClick={() => handleSelectItemType(type)}
        >
          {type}
        </Button>
      ))}
    </ItemTypesWrapper>
  );
}

export default ItemTypesComponent;
