import { useDispatch } from "react-redux";
import { addToBasket } from "../../../basket/redux/actions";
import { StyledItem, ImageWrapper, Price, Title, Button } from "./styles";

export type TProductListItem = {
  [x: string]: any;
};
const ProductListItem: React.FC<TProductListItem> = (data) => {
  const {
    data: { name, price },
  } = data;
  const dispatch = useDispatch();

  const handleAddToBasket = () => {
    dispatch(addToBasket(data.data));
  };

  const randomImage = `https://picsum.photos/600/600`;
  const getDefaultPrice = `₺`;

  return (
    <StyledItem>
      <ImageWrapper>
        <img src={randomImage} alt={name} />
      </ImageWrapper>
      <Price>
        {getDefaultPrice}
        {price}
      </Price>
      <Title>{name}</Title>
      <Button onClick={handleAddToBasket}>Add</Button>
    </StyledItem>
  );
};

export default ProductListItem;
