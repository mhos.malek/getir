/* eslint-disable testing-library/no-container */
/* eslint-disable testing-library/no-node-access */
import { render, fireEvent, screen } from "@testing-library/react";
import Item from ".";

describe("Item Component", () => {
  test("renders with props", () => {
    const mockFn = jest.fn();
    const { container } = render(
      <Item name="name" slug="slug" price="9.99" handler={mockFn} />
    );
    expect(container).toBeTruthy();
    expect(screen.getByText(/name/)).toBeTruthy();
    expect(screen.getByText(/9.99/)).toBeTruthy();
    const button = container.querySelector("button");
    fireEvent.click(button);
    expect(mockFn).toHaveBeenCalledWith({
      name: "name",
      price: "9.99",
      slug: "slug",
    });
  });
});
