import styled from "styled-components";

export const ItemsGrid = styled.div`
  width: 100%;
  display: grid;
  grid-gap: 10px;
  grid-template-columns: repeat(2, 1fr);
  @media (min-width: 768px) {
    grid-template-columns: repeat(3, 1fr);
  }
  @media (min-width: 999px) {
    grid-gap: 20px;
    grid-template-columns: repeat(4, 1fr);
  }
`;

export const Box = styled.div`
  width: 100%;
  border-radius: 2px;
  background-color: white;
  padding: 20px;
  box-shadow: 1px 1px 20px rgba(93, 62, 188, 0.04);
`;
