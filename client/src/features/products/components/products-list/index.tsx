import Loading from "../../../../components/base/loading";
import ProductListItem from "../products-list-item";
import Pagination from "../../../pagination/ui";
import { useSelector } from "react-redux";
import { RootState } from "../../../../store";
import { Box, ItemsGrid } from "./styles";
import usePageUpdate from "../../hooks/usePageUpdate";
import { useState } from "react";

import { selectPaginationInfo } from "../../../filter/redux/selector";

const ProductsList: React.FC = () => {
  const { isLoading, productsList } = useSelector(
    (state: RootState) => state.products
  );
  const paginationInfo = useSelector(selectPaginationInfo);

  const [currentPage, setCurrentPage] = useState(1);

  usePageUpdate(currentPage);

  const renderProductsList = () => {
    if (isLoading) {
      return null;
    }
    return productsList.data?.map((product) => (
      <ProductListItem key={product.slug} data={product} />
    ));
  };

  return (
    <>
      <Box>
        {isLoading && <Loading />}
        <ItemsGrid>{renderProductsList()}</ItemsGrid>
      </Box>
      {paginationInfo && (
        <Pagination
          className="pagination-bar"
          currentPage={currentPage}
          totalCount={paginationInfo.allItemsLength || 0}
          pageSize={paginationInfo.pageSize}
          onPageChange={(page) => setCurrentPage(page)}
        />
      )}
    </>
  );
};

export default ProductsList;
