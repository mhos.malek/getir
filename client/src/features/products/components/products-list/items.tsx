import Item from "../products-list-item";
import { ItemsGrid } from "./styles";

function ItemsComponent({ list = [], handler }) {
  const handle = (item) => {
    handler(item);
  };

  return (
    <div className="box">
      <ItemsGrid>
        {list.map((item) => (
          <Item
            key={item.slug}
            slug={item.slug}
            name={item.name}
            price={item.price}
            handler={handle}
          />
        ))}
      </ItemsGrid>
    </div>
  );
}

export default ItemsComponent;
