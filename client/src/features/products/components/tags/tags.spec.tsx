import { render, screen } from "@testing-library/react";
import TagsComponent from "./";
describe("Tags Component", () => {
  test("renders with props", () => {
    const { container } = render(<TagsComponent />);
    expect(container).toBeTruthy();
    expect(screen.getByText(/Tags/)).toBeTruthy();
  });
});
