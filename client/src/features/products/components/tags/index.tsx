import { useSelector, useDispatch } from "react-redux";
import { filterByTagName } from "../../../filter/redux/actions";
import { selectSelectedTags } from "../../../filter/redux/selector";

import CheckListFilter from "./check-list-filter";

const Tags = () => {
  const { tags, isLoading } = useSelector((state: any) => state.products);
  const selectedTags = useSelector(selectSelectedTags);
  const dispatch = useDispatch();

  const handleTagSelect = (tag) => {
    dispatch(filterByTagName(tag));
  };

  return (
    <CheckListFilter
      list={tags}
      selected={selectedTags}
      handler={handleTagSelect}
      loading={isLoading}
      title="Tags"
    />
  );
};

export default Tags;
