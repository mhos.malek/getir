import { useEffect, useState } from "react";

import FilterBox from "../../../../../components/common/filter-box";
import Checkbox from "../../../../../components/base/check-box";
import { addPropToArray } from "../../../../../utils";
import { FilterWrapper, CheckListrWrapper, Input } from "./styles";
import Loading from "../../../../../components/base/loading";

const CheckListFilter: React.FC<any> = ({
  list = [],
  loading = false,
  selected = [],
  handler,
  title = "",
}) => {
  const [items, setItems] = useState([]);
  const [filtered, setFiltered] = useState([]);
  const [keyword, setKeyword] = useState("");

  useEffect(() => {
    if (list.length > 0) {
      const result: any = addPropToArray(list, selected);
      setItems(result);
    }
  }, [list, selected]);

  useEffect(() => {
    if (keyword) {
      const result = items.filter((item: any) =>
        item.name.toLowerCase().includes(keyword)
      );
      setFiltered(result);
    }
  }, [keyword, items]);

  const handleChange = (e) => {
    const { value } = e.target;
    setKeyword(value);
  };

  const renderFarm = (items: any, filtered: any) => {
    const list = keyword ? filtered : items;
    const output = list.map((item: any) => (
      <Checkbox
        key={item.slug}
        slug={item.slug}
        label={item.name}
        selected={item?.selected}
        handler={handle}
      />
    ));
    return output;
  };

  const handle = (item: any) => {
    handler(item);
  };

  return (
    <FilterBox title={title}>
      <FilterWrapper>
        {loading && <Loading />}
        {!loading && (
          <>
            <Input
              type="text"
              className="input"
              placeholder={`Search ${title}`}
              name="keyword"
              value={keyword}
              onChange={handleChange}
            />
            <CheckListrWrapper>
              <Checkbox
                label="All"
                selected={selected.length === 0 || selected.includes("All")}
                handler={() => handle("All")}
              />
              {renderFarm(items, filtered)}
            </CheckListrWrapper>
          </>
        )}
      </FilterWrapper>
    </FilterBox>
  );
};

export default CheckListFilter;
