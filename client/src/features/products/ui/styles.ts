import styled from "styled-components";

export const Main = styled.main`
  order: 3;
  margin: 8px;

  @media (min-width: 999px) {
    width: 50%;
  }
  @media (min-width: 999px) {
    order: 2;
  }
`;

export const column = styled.div`
  width: 100%;
  padding: 0 8px;
`;

export const SideBar = styled.aside`
  margin: 8px;
  @media (min-width: 999px) {
    width: 25%;
  }
`;

export const filterBar = styled(SideBar)`
  order: 1;
`;
export const SideBarBaseket = styled(SideBar)`
  order: 2;
  @media (min-width: 999px) {
    order: 3;
  }
`;
