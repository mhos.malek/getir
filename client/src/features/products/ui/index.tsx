import PagesLayout from "../../../components/layout/index";
import ProductsList from "../components/products-list";
import SortSection from "../components/sort";
import Brands from "../../brands/components";
import Tags from "../components/tags";
import ItemTypesFilter from "../components/type";
import Basket from "../../basket";
import { Main, SideBar, SideBarBaseket } from "./styles";
import NarbarBasketComponent from "../../basket/components/navbar-basket/navbar-basket";

function ProductsListPage() {
  // const { t, i18n } = useTranslation();

  return (
    <PagesLayout header={<NarbarBasketComponent />}>
      <SideBar>
        <SortSection />
        <Brands />
        <Tags />
      </SideBar>
      <Main>
        <h1>products</h1>

        <ItemTypesFilter />
        <ProductsList />
      </Main>
      <SideBarBaseket>
        <Basket />
      </SideBarBaseket>
    </PagesLayout>
  );
}

export default ProductsListPage;
