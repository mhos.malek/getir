import { createSelector } from "reselect";

export const selectProductsState = (state) => state.products;

// extract tag names

export const selectTags = (state) => state.products.tags;

export const selectAllTags = createSelector(
  selectProductsState,
  selectTags,
  (products) => {
    const tagsList = products?.productsList?.data?.reduce((res, current) => {
      for (let i = 0; i < current.tags.length; i++) {
        if (!res.includes(current.tags[i])) {
          res.push(current.tags[i]);
        }
      }
      return res;
    }, []);

    let tagsListFinal = [];
    if(tagsList.length > 0){
      tagsListFinal  = tagsList.map((item) => ({
        name: item,
        slug: item.replace(/\s/g, ""),
      }));
    }
    

    return tagsListFinal;
  }
);
