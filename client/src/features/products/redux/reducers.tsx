import { sortTypes } from "../components/sort/types";
import ActionTypes from "./actionTypes";

const initial = {
  isLoading: false,
  productsList: [],
  sort: {
    order: "ascending",
    type: "price",
    key: "PriceLowToHigh",
  },
  tags: [],
};
export function reducers(state = initial, action) {
  switch (action.type) {
    case ActionTypes.FETCH_PRODUCTS_LIST:
      return {
        ...state,
        isLoading: true,
      };
    case ActionTypes.FETCH_PRODUCTS_LIST_SUCCEED:
      return { ...state, isLoading: false, ...action.payload };
    case ActionTypes.FETCH_PRODUCTS_LIST_FAILED:
      return { ...state, isLoading: false };

    case ActionTypes.SET_SELECTED_TYPE:
      return { ...state, ...action.payload };

    case ActionTypes.SET_SELECTED_SORT:
      return {
        ...state,
        isLoading: true,
        sort: {
          ...state.sort,
          type: sortTypes[action.payload.selectedSort].type,
          order: sortTypes[action.payload.selectedSort].order,
          key: action.payload.selectedSort,
        },
      };

    case ActionTypes.UPDATE_TAGS:
      return {
        ...state,
        tags: action.payload.tags,
      };
    default:
      return state;
  }
}
