import ActionTypes from "./actionTypes";

const fetchProductsList = () => ({
  type: ActionTypes.FETCH_PRODUCTS_LIST,
});

const fetchListSucceed = (productsList) => ({
  type: ActionTypes.FETCH_PRODUCTS_LIST_SUCCEED,
  payload: {
    productsList,
  },
});

const fetchListFailed = () => ({
  type: ActionTypes.FETCH_PRODUCTS_LIST_FAILED,
});

// sort
export const setSelectedSort = (selectedSort) => ({
  type: ActionTypes.SET_SELECTED_SORT,
  payload: {
    selectedSort,
  },
});

// tags

export const updateTagsList = (tags) => ({
  type: ActionTypes.UPDATE_TAGS,
  payload: {
    tags,
  },
});

export { fetchProductsList, fetchListSucceed, fetchListFailed };
