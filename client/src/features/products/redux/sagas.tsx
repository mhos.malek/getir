import { call, put, select, takeLatest } from "redux-saga/effects";
import getAllProductsAPI from "../../../services/api/products";
import { updatePaginationInfo } from "../../filter/redux/actions";
import { FilterActionTypes } from "../../filter/redux/actionTypes";
import {
  selectBrandFilter,
  selectItemTypeFilter,
  selectPageFilter,
  selectSortOrderFilter,
  selectSortTypeFilter,
  selectTagsFilter,
} from "../../filter/redux/selector";
import queryBuilder from "../utils/queryBuilder";
import {
  fetchListFailed,
  fetchListSucceed,
  fetchProductsList,
  updateTagsList,
} from "./actions";

import ActionTypes from "./actionTypes";
import { selectAllTags } from "./selector";

export function* fetchProductsListSaga() {
  try {
    const sort = yield select(selectSortTypeFilter);
    const order = yield select(selectSortOrderFilter);
    const itemType = yield select(selectItemTypeFilter);
    const tags = yield select(selectTagsFilter);
    const brands = yield select(selectBrandFilter);
    const page = yield select(selectPageFilter);

    const query = queryBuilder({
      sort,
      order,
      itemType,
      brands,
      tags,
      page,
    });

    const response = yield call(getAllProductsAPI, query);
    yield put(updatePaginationInfo(response.data.pagination));
    yield put(fetchListSucceed(response.data));
  } catch (error) {
    yield put(fetchListFailed());
  } finally {
  }
}

function* updateTagsListSaga() {
  const tags = yield select(selectAllTags);
  yield put(updateTagsList(tags));
}

function* handleChangeFiltersOrSortSaga() {
  yield put(fetchProductsList());
}

function* productSaga() {
  yield takeLatest(ActionTypes.FETCH_PRODUCTS_LIST, fetchProductsListSaga);
  yield takeLatest(
    [
      ActionTypes.SET_SELECTED_TYPE,
      FilterActionTypes.FILTER_BY_BRAND_NAME,
      FilterActionTypes.FILTER_BY_ITEM_TYPE,
      FilterActionTypes.FILTER_BY_TAG_NAME,
      FilterActionTypes.UPDATE_CURRENT_PAGE,
    ],
    handleChangeFiltersOrSortSaga
  );
  yield takeLatest(ActionTypes.FETCH_PRODUCTS_LIST_SUCCEED, updateTagsListSaga);
}

export default productSaga;
