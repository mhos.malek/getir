export const basePath = "/products";

const pathes = {
  PRODUCT_LIST: `/`,
  PRODUCT_LIST_PAGE: `${basePath}/:page`,
  NOT_FOUND: `${basePath}/*`,
  // other pathes realted to this feature
};

export default pathes;
