import ProductsListPage from "../ui";
import Pathes from "./pathes";

const ProductsRoutes = [
  // products feature related routes
  {
    path: Pathes.PRODUCT_LIST,
    component: ProductsListPage,
  },
  {
    path: Pathes.PRODUCT_LIST_PAGE,
    component: ProductsListPage,
  },
];

export default ProductsRoutes;
