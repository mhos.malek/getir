import { useMemo } from "react";
import { useDispatch } from "react-redux";
import { updatePage } from "../../filter/redux/actions";

const usePageUpdate = (currentPage) => {
  const dispatch = useDispatch();
  useMemo(() => {
    dispatch(updatePage(currentPage));
  }, [currentPage, dispatch]);
  return currentPage;
};
export default usePageUpdate;
