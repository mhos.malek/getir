const basePath = "/products";

const pathes = {
  PRODUCT_LIST: `${basePath}`,
  PRODUCT_ITEM: `${basePath}/:productId`,
  NOT_FOUND: `${basePath}/*`,
  // other pathes realted to this feature
};

export default pathes;
