import { createSelector } from "reselect";

export const selectFilterState = (state) => state.filter;

// filters and sort
export const selectItemTypeFilter = createSelector(
  selectFilterState,
  (filters) => filters.itemType
);

export const selectSelectedTags = (state) => state.filter.tags;

export const selectSortOrderFilter = (state) => state.products.sort.order;
export const selectSortTypeFilter = (state) => state.products.sort.type;
export const selectSortTypeKey = (state) => state.products.sort.key;

export const selectPageFilter = (state) => state.filter.currentPage;
export const selectPaginationInfo = (state) => state.filter.paginationInfo;

export const selectBrandFilter = (state) => state.filter.brands;
export const selectTagsFilter = (state) => state.filter.tags;
