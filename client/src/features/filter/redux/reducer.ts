import { FilterActionTypes } from "./actionTypes";

const initial = {
  brands: [],
  tags: [],
  itemType: "",
  currentPage: 0,
  paginationInfo: {
    allPages: 0,
    allItemsLength: 0,
    pageSize: 16,
  },
};
const addNewSelection = (currentState, newSelection) => {
  if (newSelection === "All") {
    return [newSelection];
  }
  if (currentState.includes(newSelection)) {
    return currentState.filter((item) => item !== newSelection);
  } else {
    return [...currentState, newSelection];
  }
};
export function reducers(state = initial, action) {
  switch (action.type) {
    case FilterActionTypes.FILTER_BY_BRAND_NAME:
      return {
        ...state,
        brands: addNewSelection(state.brands, action.payload.brandNames),
      };
    case FilterActionTypes.UPDATE_PAGINATION_INFO:
      return {
        ...state,
        paginationInfo: action.payload,
      };
    case FilterActionTypes.UPDATE_CURRENT_PAGE:
      return {
        ...state,
        currentPage: action.payload,
      };
    case FilterActionTypes.FILTER_BY_ITEM_TYPE:
      return {
        ...state,
        itemType: action.payload.itemType,
      };
    case FilterActionTypes.FILTER_BY_TAG_NAME:
      return {
        ...state,
        tags: addNewSelection(state.tags, action.payload.tagNames),
      };
    default:
      return state;
  }
}
