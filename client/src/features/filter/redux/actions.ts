import { FilterActionTypes } from "./actionTypes";

export const filterByBrandName = (brandNames) => ({
  type: FilterActionTypes.FILTER_BY_BRAND_NAME,
  payload: {
    brandNames,
  },
});
export const filterByTagName = (tagNames) => ({
  type: FilterActionTypes.FILTER_BY_TAG_NAME,
  payload: {
    tagNames,
  },
});

export const filterByItemType = (itemType) => ({
  type: FilterActionTypes.FILTER_BY_ITEM_TYPE,
  payload: {
    itemType,
  },
});

export type TUpdatePaginationInfoAction = (page: Record<string, any>) => {
  type: FilterActionTypes.UPDATE_PAGINATION_INFO;
  payload: Record<string, any>;
};

export type TUpdatePageAction = (page) => {
  type: FilterActionTypes.UPDATE_CURRENT_PAGE;
  payload: Record<string, any>;
};

export const updatePaginationInfo: TUpdatePaginationInfoAction = (
  paginationInfo
) => ({
  type: FilterActionTypes.UPDATE_PAGINATION_INFO,
  payload: paginationInfo,
});

export const updatePage: TUpdatePageAction = (page) => ({
  type: FilterActionTypes.UPDATE_CURRENT_PAGE,
  payload: page,
});

export type TActionTtypes =
  TUpdatePaginationInfoAction; /* | TUpdateFilterByItemTypeAction ... and other actions that need to be completed */
